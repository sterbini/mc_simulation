# MC_simulation



## Getting started
Go to your preferred folder and do the follwing
```bash
 wget https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-x86_64.sh
 bash Miniforge3-Linux-x86_64.sh
 source miniforge3/bin/activate
 pip install numpy scipy matplotlib pandas ipython pytest psutil memory_profiler
 git clone https://github.com/xsuite/tree_maker.git
 cd tree_maker
 git checkout release/v0.1.0
 python -m pip install -e .
 cd ../
 git clone ssh://git@gitlab.cern.ch:7999/mrufolo/data_analysis_vs.git
 cd data_analysis_vs
 pip install -e .
```
