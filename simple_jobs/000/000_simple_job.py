from bb_toolbox import data_synthesis as ds
# import json
import yaml
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt 
#from cpu_load_generator import load_single_core, load_all_cores, from_profile


######
with open('config.yaml', 'r') as file:
    cfg = yaml.safe_load(file)

# Start tree_maker logging if log_file is present in config
try:
    import tree_maker
    if 'log_file' not in cfg.keys():
        tree_maker = None
except:
    tree_maker = None

if tree_maker is not None:
    tree_maker.tag_json.tag_it(cfg['log_file'], 'started')

#load_single_core(core_num=cfg['core'], duration_s=20, target_load=1) # generate load on single core (0)


module_name, function_name = cfg['function_boundary'].split('.')

tool_box = __import__(module_name)
search_boundary = getattr(tool_box, function_name)

data = search_boundary(cfg['parquet_filename'])
print((data))


#f = open(cfg['parquet_filename'])
#data = json.load(f)

for i in range(len(data)):
    slots_to_shift = data[i]['slots_to_shift']
    #print(slots_to_shift)
    slots_init = data[i]['slots_init']
#print(slots_init)
    beam = data[i]['beam']
#print(beam)
    twelve_B1 = slots_to_shift[0]-11
    ABK = 3383-slots_to_shift[-1]+1
    df = ds.MC_shift_new(slots_to_shift, slots_init, beam, cfg['number_of_iterations'],36,twelve_B1 = twelve_B1,ABK_B1 = ABK)
    if i == 0:
        tot_df = df
    else:
        tot_df = pd.concat([tot_df,df], ignore_index=True)
    tot_df.to_parquet(f'output_df.parquet')
    #df.to_parquet(f'input.parquet')


if tree_maker is not None:
    tree_maker.tag_json.tag_it(cfg['log_file'], 'completed')
#print(df)
